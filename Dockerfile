FROM node:13.10.1-alpine3.10
WORKDIR /app
COPY . /app
RUN npm install
EXPOSE 5000
CMD [ "node", "app.js" ]